require 'nokogiri'
require 'kramdown'

Nanoc::Filter.define(:md_links_to_html) do |content, params|
  dom = Nokogiri::HTML(content)
  dom.css('a').each do |a|
    uri = URI(a['href'])
    unless uri.host
      uri.path.sub!(/\.md$/, '.html')
      a['href'] = uri.to_s
    end
  end
  dom.to_html
end

module Custom
  def create_tree(tree_name)
    @config[:view_types].each_pair do |file, view_type|
      attributes = { tree: tree_name,
                     current_tree_view: file,
                     current_tree_view_type: view_type }
      item = @items.create("", attributes, "/tree/#{tree_name}/#{file}")
    end
  end

  def create_pre_merge_templates(tree_name)
    @config[:view_types].each_pair do |file, view_type|
      attributes = { tree: tree_name,
                     current_tree_view: file,
                     current_tree_view_type: view_type,
                     pre_merge: true }
      @items.create("", attributes, "/tree/#{tree_name}/pre-merge-templates/#{file}")
    end

    attributes = { tree: tree_name,
                   current_tree_view: 'filelist.html',
                   current_tree_view_type: 'filelist',
                   pre_merge: true }
    @items.create("", attributes, "/tree/#{tree_name}/pre-merge-templates/filelist.html")
  end

  def create_post_merge_templates(tree_name)
    @config[:view_types].each_pair do |file, view_type|
      attributes = { tree: tree_name,
                     current_tree_view: file,
                     current_tree_view_type: view_type,
                     pre_merge: true }
      @items.create("", attributes, "/tree/#{tree_name}/post-merge-templates/#{file}")
    end

    attributes = { tree: tree_name,
                   current_tree_view: 'filelist.html',
                   current_tree_view_type: 'filelist',
                   pre_merge: true }
    @items.create("", attributes, "/tree/#{tree_name}/post-merge-templates/filelist.html")
  end

  def create_toc_and_title_from_headings(item)
    html = Kramdown::Document.new(item.raw_content).to_html

    dom = Nokogiri::HTML(html)
    h1 = dom.css('h1').first
    item[:title] = h1 ? h1.text : 'Untitlted'

    item[:toc] = dom.css('h1, h2, h3, h4, h5, h6').map do |elem|
      { level: elem.name[1].to_i,
        anchor: elem['id'],
        text: elem.text }
    end
  end
end

module HardwareList
  def read_descriptions
    descriptions_path = 'hardware-descriptions.yml'
    return {} unless File.exist? descriptions_path
    YAML.load(File.read(descriptions_path))
  end

  def hw_description_to_pretty_yaml(data)
    data.select {|k,_| k != 'filelist'}
      .to_yaml
      .lines[1..-1] # we wan't to skip YAML header
      .join
  end

  def create_files(host, filelist)
    filenames = []
    filelist.each_pair do |file, contents|
      filename = file.end_with?('.txt') ? file : "#{file}.txt"
      items.create(contents, {}, "/hardware/#{host}/#{filename}")
      filenames << "#{contents.size} #{filename}"
    end

    items.create(filenames.join("\n"), {}, "/hardware/#{host}/filelist.txt")
    items.create("", { hw_list: true }, "/hardware/#{host}/filelist.html")
  end

  def hw_to_yaml(hw_raw)
    hw_raw.each_value do |v|
      v.delete('filelist')
    end

    hw_raw.to_yaml
  end

  def create_hardware_list(items)
    descriptions = read_descriptions
    hw_raw = HWParse.parse('../hardware')
    hw = hw_raw
      .group_by {|x| x[0].start_with?("fi-") ? x[-1]['gen'] : x[0].split('-')[0]}
      .sort_by {|x| x.first.is_a?(String) ? 0 : x.first } # put shards and pigs last
      .reverse # newest first

    content = "# Hardware List\n\n"
    content += "[raw yaml](/hardware.yml)\n\n"
    hw.each do |gen|
      if gen.first.is_a? Numeric
        content += "## Gen#{gen.first}\n\n"
      else
        content += "## #{gen.first}s\n\n"
      end
      gen.last.to_h.each_pair do |host, data|
        create_files(host, data['filelist'])

        content += "### #{host}\n"
        if descriptions[host]
          content += "#{descriptions[host]}"
        else
					content += "&nbsp;"
				end

        content += "<span style=\"float: right;\">[raw data](/hardware/#{host}/filelist.html)</span>\n"
        content += "<pre>#{hw_description_to_pretty_yaml(data)}</pre>\n\n"
      end
    end

    items.create(hw_to_yaml(hw_raw), {}, '/hardware.yml')
    items.create(content, {}, '/hardware.md')
  end
end

use_helper Custom
use_helper HardwareList
use_helper Nanoc::Helpers::Rendering
