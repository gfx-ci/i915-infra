<%
from datetime import datetime
import os
import re

re_pstore=re.compile("pstore%d-(.*)_[0-9]*.(log|txt)$" % test['run'])

filelist = [ ("..","integration-manifest.log"), ("..","git-log-oneline.log"),
             ("..","integration-manifest.txt"), ("..","git-log-oneline.txt") ]

run = str(test['run']) if 'run' in test else ""
filelist.append(("i915_display_info%s.txt" % run,))
filelist.append(("igt_runner%s.txt" % run,))
filelist.append(("run%s.txt" % run,))
filelist.append(("hostdmesg%s.txt" % run,))
filelist.append(("runtimes%s.txt" % run,))
filelist.append(("results%s.json.bz2" % run,))
filelist.append(("results%s-xe-load.json.bz2" % run,))
filelist.append(("results%s-i915-load.json.bz2" % run,))
filelist.append(("i915_display_info_post_exec%s.txt" % run,))
filelist.append(("serial_data%s.txt" % run,))
try:
    filelist.append((test['boot_file'],))
except KeyError:
    filelist.append(("boot%s.txt" % run,))
try:
    filelist.append((test['dmesg_file'],))
except KeyError:
    filelist.append(("dmesg%s.txt" % run,))
%>
<?xml version="1.0" encoding="UTF-8"?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="format-detection" content="telephone=no" />
    <title>${title} Test details</title>
    <link rel="stylesheet" href="/assets/test_result.css" type="text/css" />
    <link rel="stylesheet" href="/assets/results.css" type="text/css" />
    <script type="text/javascript" src="/assets/test_result.js"></script>
    <link rel="shortcut icon" href="/assets/favicon.gif" />
    <style>
        .legend {
            float: none;
            position: sticky;
            cursor: default;
        }
    </style>
  </head>
  <body>
    <h1>Results for ${testname}</h1>
    <div>
      <p class="legend"><b>Result:</b> <span class="button ${test['result']}">${test['result'].title()}</span>
        % if 'dmesg_warning_lines' in test.keys():
          <a href="#dmesg-warnings${test['dmesg_warning_lines'][0]}"
             title="Scroll to first warning"
             class="button dmesg-warn">${len(test['dmesg_warning_lines'])} Warning(s)</a></div>
        % endif
      </p>
    </div>
    <p>
    % for filetuple in filelist:
      % if os.path.isfile(os.path.join(path, *filetuple)):
        <a href="${'/'.join(filetuple)}">${os.path.splitext(filetuple[-1])[0]}</a>
      % endif
    % endfor
    </p>
    % for file in os.listdir(path):
      % if re.search(re_pstore, file):
        <a href="${file}">${os.path.splitext(file)[0]}</a>
      % endif
    % endfor
    </p>
    <table>
      <tr><th>Detail</th><th>Value</th></tr>
      % if 'time' in test:
      <tr><td>Duration</td><td>
      % if test['result'] == "incomplete" or test['result'] == "abort":
        unknown
      % else:
        ${"%4.2f seconds" % (test['time']['end']-test['time']['start'])}
      % endif
      </b></tr>
      % endif
    % for var in ['hostname', 'returncode', 'igt-version', 'out', 'err', 'environment', 'command', 'exception', 'traceback', 'dmesg']:
      % if var in test and test[var]:
      <tr><td>${var.title()}
             % if var == 'dmesg' and 'dmesg-warnings' and 'dmesg_warning_lines' in test.keys():
               </br></br><div class="legend"><a href="#dmesg-warnings${test['dmesg_warning_lines'][0]}"
                            title="Scroll to first warning"
                            class="button dmesg-warn">Scroll to first warning</a>
             % endif
          </td>
          <td>
              % if var == 'dmesg':
                % for idx, dmesg in enumerate(test[var]):
                  % if 'dmesg_warning_lines' in test.keys() and idx in test['dmesg_warning_lines']:
                    <div><span id="dmesg-warnings${idx}" class="dmesg dmesg-warnings">${dmesg | h}</span></div>
                  % else:
                    <div><span id="dmesg-normal${idx}" class="dmesg dmesg-normal">${dmesg | h}</span></div>
                  % endif
                % endfor
              % else:
                <pre>${test[var] | h}</pre>
              % endif
          </td>
      </tr>
      % endif
    %endfor  
    </table>

    ## Highlighting logs with [IGT] in them
    <script>
        document.querySelectorAll("span.dmesg").forEach(x =>
            x.textContent.includes("[IGT]") && (x.classList.add("dmesg-igt"))
        );
    </script>

    <i>Created at ${datetime.now().strftime('%Y-%m-%d %H:%M:%S')}</i>
  </body>
</html>
