# WARNING: DO NOT EDIT THESE FILES MANUALLY!

Files in this directory should not be edited manually - they are copied from
[Xe CI kconfigs](https://gitlab.freedesktop.org/drm/xe/ci/-/tree/main/kernel).
Use the kconfigs from that repo for both i915 and Xe work - this location will
go away once the entire i915 CI is updated to use the new location.

You should not edit any of these manually, CI folks should keep them in sync
and up to date as long as they are needed. If you need any changes here, make
them in the repo linked above instead.
